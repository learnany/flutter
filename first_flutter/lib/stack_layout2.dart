import 'package:flutter/material.dart';

class StackLayout2 extends StatelessWidget {
  const StackLayout2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("层叠布局2"),
        ),
        body: Center(
            child: //通过ConstrainedBox来确保Stack占满屏幕
            ConstrainedBox(
              constraints: BoxConstraints.expand(),
              child: Stack(
                alignment:Alignment.center ,
                fit: StackFit.expand, //未定位widget占满Stack整个空间
                children: <Widget>[
                  Positioned(
                    left: 18.0,
                    child: Text("I am Jack"),
                  ),
                  Container(child: Text("Hello world",style: TextStyle(color: Colors.white)),
                    color: Colors.red,
                  ),
                  Positioned(
                    top: 18.0,
                    child: Text("Your friend"),
                  )
                ],
              ),
            )
        )
    );
  }
}