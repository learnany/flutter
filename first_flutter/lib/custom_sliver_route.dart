import 'package:flutter/material.dart';

class CustomSliverRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SliverFlexibleHeader"),
      ),
      body: CustomScrollView(
          //为了能使CustomScrollView拉到顶部时还能继续往下拉，必须让 physics 支持弹性效果
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          slivers: [
            buildSliverList(),
          ],
      ),
    );
  }


  // 构建固定高度的SliverList，count为列表项属相
  Widget buildSliverList([int count = 5]) {
    return SliverFixedExtentList(
      itemExtent: 50,
      delegate: SliverChildBuilderDelegate(
            (context, index) {
          return ListTile(title: Text('$index'));
        },
        childCount: count,
      ),
    );
  }
}