import 'package:flutter/material.dart';

class TextAndStyle extends StatelessWidget {
  const TextAndStyle({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("文本及样式"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text("Hello world",
            textAlign: TextAlign.left,
          ),
          Text("Hello world! I'm Jack. "*4,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          const Text("Hello world",
            textScaleFactor: 1.5,
          ),
          Text("Hello world "*6,// 字符串重复六次
            textAlign: TextAlign.center,
          ),
          Text("Hello world",
            style: TextStyle(
                color: Colors.blue,
                fontSize: 18.0,
                height: 1.2,
                fontFamily: "Courier",
                background: Paint()..color=Colors.yellow,
                decoration:TextDecoration.underline,
                decorationStyle: TextDecorationStyle.dashed
            )
          ),
          const Text.rich(TextSpan(
              children: [
                TextSpan(
                    text: "Home: "
                ),
                TextSpan(
                    text: "https://flutterchina.club",
                    style: TextStyle(
                        color: Colors.blue
                    )
                ),
              ]
          )),
          DefaultTextStyle(
            //1.设置文本默认样式
            style: const TextStyle(
              color:Colors.lightBlue,
              fontSize: 20.0,
            ),
            textAlign: TextAlign.start,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Text("hello world"),
                Text("I am Jack"),
                Text("I am Jack",
                  style: TextStyle(
                      inherit: false, //2.不继承默认样式
                      color: Colors.grey
                  ),
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}