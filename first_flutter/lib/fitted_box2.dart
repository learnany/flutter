import 'package:flutter/material.dart';

import 'layout_builder2.dart';

class SingleLineFittedBox extends StatelessWidget {
  const SingleLineFittedBox({Key? key,this.child}) : super(key: key);
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, constraints) {
        return FittedBox(
          child: ConstrainedBox(
            constraints: constraints.copyWith(
              //让 maxWidth 使用屏幕宽度
              minWidth: constraints.maxWidth,
              maxWidth: double.infinity,
            ),
            child: child,
          ),
        );
      },
    );
  }
}

class FittedBox2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("FittedBox示例"),
      ),
      body: Center(
        child: Column(
          children: [
            wContainer(BoxFit.none),
            Text('Wendux'),
            wContainer(BoxFit.contain),
            Text('Flutter中国'),
            Column(
              children:  [
                wRow(' 90000000000000000 '),
                SingleLineFittedBox(child: wRow(' 90000000000000000 ')),
                LayoutLogPrint(tag: 1, child: wRow(' 800 ')),
                SingleLineFittedBox(child: LayoutLogPrint(tag: 2, child: wRow(' 800 '))),
              ].map((e) => Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: e,
              )).toList(),
            )
          ],
        ),
      ),
    );
  }

  Widget wContainer(BoxFit boxFit) {
    return ClipRect(
      child: Container(
        width: 50,
        height: 50,
        color: Colors.red,
        child: FittedBox(
          fit: boxFit,
          // 子容器超过父容器大小
          child: Container(width: 60, height: 70, color: Colors.blue),
        ),
      ),
    );
  }

  // 直接使用Row
  Widget wRow(String text) {
    Widget child = Text(text);
    child = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [child, child, child],
    );
    return child;
  }
}
