import 'package:flutter/material.dart';
class AnimatedDecoratedBox extends ImplicitlyAnimatedWidget {
  const AnimatedDecoratedBox({
    Key? key,
    required this.decoration,
    required this.child,
    Curve curve = Curves.linear,
    required Duration duration,
  }) : super(
    key: key,
    curve: curve,
    duration: duration,
  );
  final BoxDecoration decoration;
  final Widget child;

  @override
  _AnimatedDecoratedBoxState createState() {
    return _AnimatedDecoratedBoxState();
  }
}
class _AnimatedDecoratedBoxState
    extends AnimatedWidgetBaseState<AnimatedDecoratedBox> {
  late DecorationTween _decoration;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: _decoration.evaluate(animation),
      child: widget.child,
    );
  }

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _decoration = visitor(
      _decoration,
      widget.decoration,
          (value) => DecorationTween(begin: value),
    ) as DecorationTween;
  }
}

class AnimatedDecoratedBoxRoute2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AnimatedDecoratedBoxRouteState2();
  }

}

class AnimatedDecoratedBoxRouteState2 extends State<AnimatedDecoratedBoxRoute2> {

  Color _decorationColor = Colors.blue;
  var duration = Duration(seconds: 1);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("动画过渡2"),
      ),
      body: AnimatedDecoratedBox(
        duration: duration,
        decoration: BoxDecoration(color: _decorationColor),
        child: TextButton(
          onPressed: () {
            setState(() {
              _decorationColor = Colors.red;
            });
          },
          child: const Text(
            "AnimatedDecoratedBox",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

}