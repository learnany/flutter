import 'package:flutter/material.dart';

class AlignLayout extends StatelessWidget {
  const AlignLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Align示例"),
        ),
        body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 120.0,
                  width: 120.0,
                  color: Colors.blue.shade50,
                  child: const Align(
                    alignment: Alignment.topRight,
                    child: FlutterLogo(
                      size: 60,
                    ),
                  ),
                ),
                Container(
                  color: Colors.blue.shade50,
                  child: const Align(
                    alignment: Alignment.topRight,
                    widthFactor: 2,
                    heightFactor: 2,
                    child: FlutterLogo(
                      size: 60,
                    ),
                  ),
                ),
                Container(
                  color: Colors.blue.shade50,
                  child: const Align(
                    alignment: Alignment(2,0.0),
                    widthFactor: 2,
                    heightFactor: 2,
                    child: FlutterLogo(
                      size: 60,
                    ),
                  ),
                ),
                Container(
                  height: 120.0,
                  width: 120.0,
                  color: Colors.blue[50],
                  child: Align(
                    alignment: FractionalOffset(0.2, 0.6),
                    child: FlutterLogo(
                      size: 60,
                    ),
                  ),
                )
              ],
            )
        )
    );
  }
}