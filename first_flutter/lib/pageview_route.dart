import 'package:flutter/material.dart';
import 'package:firstflutter/keep_alive_route.dart';
// Tab 页面
class Page2 extends StatefulWidget {
  const Page2({
    Key? key,
    required this.text
  }) : super(key: key);

  final String text;

  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  @override
  Widget build(BuildContext context) {
    print("build ${widget.text}");
    return Center(child: Text("${widget.text}", textScaleFactor: 5));
  }
}

class PageViewRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];
    // 生成 6 个 Tab 页
    for (int i = 0; i < 6; ++i) {
      // children.add( Page2( text: '$i'));
      //只需要用 KeepAliveWrapper 包装一下即可
      children.add(KeepAliveWrapper(child:Page2( text: '$i')));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("PageView示例"),
      ),
      body: PageView(
        // scrollDirection: Axis.vertical, // 滑动方向为垂直方向
        children: children,
        allowImplicitScrolling: true,
      ),
    );
  }
}