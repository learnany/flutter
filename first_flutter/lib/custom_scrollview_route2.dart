import 'package:flutter/material.dart';

class CustomScrollViewRoute2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return // 因为本路由没有使用 Scaffold，为了让子级Widget(如Text)使用
// Material Design 默认的样式风格,我们使用 Material 作为本路由的根。
      Material(
        child: CustomScrollView(
          slivers: <Widget>[
            // AppBar，包含一个导航栏.
            SliverAppBar(
              pinned: true, // 滑动到顶端时会固定住
              expandedHeight: 250.0,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text('CustomScrollView示例2'),
                background: Image.asset(
                  "./images/images.jpeg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(8.0),
              sliver: SliverGrid(
                //Grid
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, //Grid按两列显示
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  childAspectRatio: 4.0,
                ),
                delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    //创建子widget
                    return Container(
                      alignment: Alignment.center,
                      color: Colors.cyan[100 * (index % 9)],
                      child: Text('grid item $index'),
                    );
                  },
                  childCount: 20,
                ),
              ),
            ),
            SliverFixedExtentList(
              itemExtent: 50.0,
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  //创建列表项
                  return Container(
                    alignment: Alignment.center,
                    color: Colors.lightBlue[100 * (index % 9)],
                    child: Text('list item $index'),
                  );
                },
                childCount: 20,
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 300,
                child: PageView(
                  children: [Text("1"), Text("2")],
                ),
              ),
            ),
          ],
        ),
      );
  }
}