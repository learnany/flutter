import 'package:flutter/material.dart';
import 'package:firstflutter/SwitchAndCheckBox.dart';

class ImageStyle extends StatelessWidget {
  const ImageStyle({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("图片样式&Switch&Che"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const <Widget>[
            Image(
              image: AssetImage("images/avatar.jpeg"),
              width: 100.0
            ),
            Image(
              image: NetworkImage(
                  "https://avatars2.githubusercontent.com/u/20411648?s=460&v=4"),
              width: 100.0,
            ),
            SwitchAndCheckBox()
          ],

        )
    );
  }
}