import 'package:firstflutter/LoginInput.dart';
import 'package:firstflutter/animated_decorated_box_route.dart';
import 'package:firstflutter/animated_decorated_box_route1.dart';
import 'package:firstflutter/animated_list_route.dart';
import 'package:firstflutter/animated_widgets_route.dart';
import 'package:firstflutter/animation_route.dart';
import 'package:firstflutter/box_constraints2.dart';
import 'package:firstflutter/custom_scrollview_route.dart';
import 'package:firstflutter/custom_scrollview_route2.dart';
import 'package:firstflutter/custom_scrollview_route3.dart';
import 'package:firstflutter/custom_sliver_route.dart';
import 'package:firstflutter/dio_route.dart';
import 'package:firstflutter/draw_chess_board.dart';
import 'package:firstflutter/file_operation_route.dart';
import 'package:firstflutter/flex_box_layout.dart';
import 'package:firstflutter/gradient_button_route.dart';
import 'package:firstflutter/gradient_circular_progress_indicator.dart';
import 'package:firstflutter/gridview_route.dart';
import 'package:firstflutter/hero_route.dart';
import 'package:firstflutter/http_client_route.dart';
import 'package:firstflutter/inherited_widget_route.dart';
import 'package:firstflutter/keep_alive_route.dart';
import 'package:firstflutter/listview_route.dart';
import 'package:firstflutter/listview_route3.dart';
import 'package:firstflutter/my_slide_transition.dart';
import 'package:firstflutter/pageview_route.dart';
import 'package:firstflutter/scroll_controller_route.dart';
import 'package:firstflutter/scroll_controller_route2.dart';
import 'package:firstflutter/stagger_animation_route.dart';
import 'package:firstflutter/switch_view_dart.dart';
import 'package:firstflutter/tabbarview_route.dart';
import 'package:firstflutter/turn_box_route.dart';
import 'package:firstflutter/will_pop_scope_route.dart';
import 'package:firstflutter/wrap_layout.dart';
import 'package:flutter/material.dart';
import 'package:firstflutter/textandstyle.dart';
import 'package:firstflutter/btnstyle.dart';
import 'package:firstflutter/imagestyle.dart';
import 'package:firstflutter/progress_indicator.dart';
import 'package:firstflutter/linear_layout.dart';
import 'package:firstflutter/align_layout.dart';
import 'package:firstflutter/clip2.dart';
import 'package:firstflutter/container2.dart';
import 'package:firstflutter/decorated_box2.dart';
import 'package:firstflutter/layout_builder2.dart';
import 'package:firstflutter/padding2.dart';
import 'package:firstflutter/transform2.dart';
import 'package:firstflutter/stack_layout1.dart';
import 'package:firstflutter/stack_layout2.dart';
import 'package:firstflutter/fitted_box2.dart';
import 'package:firstflutter/scaffold_route.dart';
import 'package:firstflutter/scrollview_route.dart';
import 'package:firstflutter/scrollview_route.dart';

import 'fade_route.dart';
import 'listview_route2.dart';
import 'listview_route4.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Scrollbar( // 显示进度条
          child: SingleChildScrollView(
            padding: EdgeInsets.all(0.0),
            child: Wrap(
              children: [
                TextButton(
                  child: const Text("文本及样式"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const TextAndStyle();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("按钮样式"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const BtnStyle();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("图片样式&Switch&CheckBox"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const ImageStyle();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("登录输入框"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return LoginInput();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("进度指示器"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ProgressIndicator2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("BoxConstraints2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return BoxConstraints2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("线性布局"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return LinearLayout();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("弹性布局"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return FlexBoxLayout();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("WrapLayout"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return WrapLayout();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("层叠布局1"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return StackLayout1();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("层叠布局2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return StackLayout2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Align示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AlignLayout();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("LayoutBuilder示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return LayoutBuilder2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Padding示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return Padding2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("DecoratedBox示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return DecoratedBox2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Transform示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return Transform2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Container示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return Container2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Clip示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return Clip2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("FittedBox示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return FittedBox2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Scaffold示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ScaffoldRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("ScrollView示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ScrollViewRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("ListView示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ListViewRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("ListView.separated"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ListViewRoute2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("ListView.builder"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ListViewRoute3();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("ListView示例2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return InfiniteListView();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("滚动监听示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ScrollControllerTestRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("滚动监听示例2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ScrollNotificationTestRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("AnimatedList示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedListRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("GridView示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return InfiniteGridView();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("PageView示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return PageViewRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("KeepAlive示例"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return KeepAliveRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("TabbarView"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return TabViewRoute2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("合并两个list"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return CustomScrollViewRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("CustomScrollView示例2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return CustomScrollViewRoute2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("CustomScrollView示例3"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return CustomScrollViewRoute3();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("SliverFlexibleHeader"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return CustomSliverRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("WillPopScope"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return WillPopScopeRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("InheritedWidget"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return InheritedWidgetTestRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("基础动画"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ScaleAnimationRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("渐隐渐入过渡"),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   PageRouteBuilder(
                    //     transitionDuration: const Duration(milliseconds: 500), //动画时间为500毫秒
                    //     pageBuilder: (BuildContext context, Animation<double> animation,
                    //         Animation secondaryAnimation) {
                    //       return FadeTransition(
                    //         //使用渐隐渐入过渡,
                    //         opacity: animation,
                    //         child: ScaleAnimationRoute(), //路由B
                    //       );
                    //     },
                    //   ),
                    // );
                    //使用FadeRoute
                    Navigator.push(context, FadeRoute(Colors.red, "1111111111", builder: (BuildContext context) {
                      return ScaleAnimationRoute();
                    }, ));
                  },
                ),
                TextButton(
                  child: const Text("Hero"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return HeroAnimationRouteA();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("交织动画"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return StaggerRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("视图切换"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedSwitcherCounterRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("视图切换2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedSwitcherCounterRoute2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("过渡动画"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedDecoratedBoxRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("过渡动画2"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedDecoratedBoxRoute2();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("过渡动画3"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return AnimatedWidgetsTest();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("GradientButton"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return GradientButtonRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("TurnBox"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return TurnBoxRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("画棋盘"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return DrawChessBoardRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("渐变进度"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return GradientCircularProgressRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("文件操作"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return FileOperationRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("HttpClient"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return HttpTestRoute();
                      }),
                    );
                  },
                ),
                TextButton(
                  child: const Text("Dio"),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return DioRoute();
                      }),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
