import 'package:flutter/material.dart';

class LinearColumn extends StatelessWidget {
  const LinearColumn({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Column示例"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const <Widget>[
            Text("hi"),
            Text("world"),
          ],
        )
    );
  }
}