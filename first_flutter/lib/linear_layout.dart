import 'package:firstflutter/linear_row.dart';
import 'package:firstflutter/linear_column.dart';
import 'package:firstflutter/linear_column2.dart';
import 'package:firstflutter/linear_column3.dart';
import 'package:flutter/material.dart';

class LinearLayout extends StatelessWidget {
  const LinearLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("线性布局"),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextButton(
                child: const Text("Row示例"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return LinearRow();
                    }),
                  );
                },
              ),
              TextButton(
                child: const Text("Column示例"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return LinearColumn();
                    }),
                  );
                },
              ),
              TextButton(
                child: const Text("Column示例2"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return LinearColumn2();
                    }),
                  );
                },
              ),
              TextButton(
                child: const Text("Column示例3"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return LinearColumn3();
                    }),
                  );
                },
              )
            ],
          )
        )
    );
  }
}