import 'package:flutter/material.dart';
import 'package:go_moon/widgets/custom_dropdown_button_class.dart';

class HomePage extends StatelessWidget {
  late double _deviceHeight, _deviceWidth;

  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: _astroImageWidget(),
          ),
          SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: _deviceWidth * 0.05),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [_pageTitle(), _bookRideWidget()],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container _astroImageWidget() {
    return Container(
      height: _deviceHeight * 1.0,
      width: _deviceHeight * 0.65,
      decoration: const BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fitHeight,
              image: AssetImage("assets/images/flutter_go_moon_02.png"))),
    );
  }

  Widget _pageTitle() {
    return const Text(
      "#GoMoon",
      style: TextStyle(
          color: Colors.white, fontSize: 70, fontWeight: FontWeight.w800),
    );
  }

  Widget _destionationDropDownWidget() {
    return CustomDropDownButtonClass(values: const [
      'James Webb Station',
      'Preneure Station',
    ], width: _deviceWidth);
  }

  Widget _travellersInfomationWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CustomDropDownButtonClass(
            values: const ['1', '2', '3', '4', '5'],
            width: _deviceWidth * 0.45),
        CustomDropDownButtonClass(
            values: const ['Economy', 'Business', 'First', 'Private'],
            width: _deviceWidth * 0.40),
      ],
    );
  }

  Widget _bookRideWidget() {
    return SizedBox(
      height: _deviceHeight * 0.25,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _destionationDropDownWidget(),
          _travellersInfomationWidget(),
          _rideButton()
        ],
      ),
    );
  }

  Widget _rideButton() {
    return Container(
      padding: EdgeInsets.only(bottom: _deviceHeight * 0.01),
      width: _deviceWidth,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: MaterialButton(
        onPressed: () {},
        child: const Text(
          "Book Ride!",
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}
