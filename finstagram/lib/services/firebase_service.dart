import 'dart:io';

// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_storage/firebase_storage.dart';

final String USER_COLLECTION = 'users';
final String POSTS_COLLECTION = 'posts';

class FirebaseService {
  // FirebaseAuth _auth = FirebaseAuth.instance;
  // FirebaseStorage _storage = FirebaseStorage.instance;
  // FirebaseFirestore _db = FirebaseFirestore.instance;
  // Map? currentUser
  Map? currentUser = {
    "image":
        "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg"
  };

  FirebaseService();

  // Future<bool> registerUser(
  //     {required String name,
  //     required String email,
  //     required String password,
  //     required File image}) async {
  //   try {
  //     UserCredential _userCredential = await _auth
  //         .createUserWithEmailAndPassword(email: email, password: password);
  //     String _userId = _userCredential.user!.uid;
  //     String _fileName = Timestamp.now().millisecondsSinceEpoch.toString() +
  //         p.extension(image.path);
  //     UploadTask _task =
  //         _storage.ref('images/$_userId/$_fileName').putFile(image);
  //     return _task.then((_snapshot) async {
  //       String _downloadURL = await _snapshot.ref.getDownloadURL();
  //       await _db
  //           .collection(USER_COLLECTION)
  //           .doc(_userId)
  //           .set({"name": name, "email": email, "image": _downloadURL});
  //     });
  //   } catch (e) {
  //     print(e);
  //     return false;
  //   }
  // }

  // Future<bool> loginUser(
  //     {required String email, required String password}) async {
  //   try {
  //     UserCredential _userCredential = await _auth.signInWithEmailAndPassword(
  //         email: email, password: password);
  //     if (_userCredential.user != null) {
  //       currentUser = await getUserData(uid: _userCredential.user!.uid);
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   } catch (e) {
  //     print(e);
  //     return false;
  //   }
  // }

  // Future<Map> getUserData({required String uid}) async {
  //   DocumentSnapshot _doc =
  //       await _db.collection(USER_COLLECTION).doc(uid).get();
  //   return _doc.data() as Map;
  // }

  // Future<bool> postImage(File _image) async {
  //   try {
  //     String _userId = _auth.currentUser!.uid;
  //     String _fileName = Timestamp.now().millisecondsSinceEpoch.toString() +
  //         p.extension(_image.path);
  //     UploadTask _task =
  //         _storage.ref('images/$_userId/$_fileName').putFile(_image);
  //     return _task.then((_snapshot) async {
  //       String _downloadURL = await _snapshot.ref.getDownloadURL();
  //       await _db.collection(POSTS_COLLECTION).add({
  //         "useId": _userId,
  //         "timestamp": Timestamp.now(),
  //         "image": _downloadURL
  //       });
  //       return true;
  //     });
  //   } catch (e) {
  //     print(e);
  //     return false;
  //   }
  // }

  // Stream<QuerySnapshot> getLastestPosts() {
  //   return _db
  //       .collection(POSTS_COLLECTION)
  //       .orderBy('timestamp', descending: true)
  //       .snapshots();
  // }

  // Stream<QuerySnapshot> getPostsForUser() {
  //   String _userID = _auth.currentUser!.uid;
  //   return _db
  //       .collection(POSTS_COLLECTION)
  //       .where('userId', isEqualTo: _userID)
  //       .snapshots();
  // }

  // Future<void> logout() async {
  //   await _auth.signOut();
  // }

  Stream<List<String>> getLastestPosts() {
    return Stream<List<String>>.fromFutures([
      Future.delayed(Duration(seconds: 1), () {
        return [
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg"
        ];
      })
    ]);
  }

  Stream<List<String>> getPostsForUser() {
    return Stream<List<String>>.fromFutures([
      Future.delayed(Duration(seconds: 1), () {
        return [
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg",
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg",
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg",
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg",
          "https://marketplace.canva.cn/EAGE6DsWb24/1/0/1600w/canva-cFjPThAyvu8.jpg"
        ];
      })
    ]);
  }

  Future<bool> registerUser(
      {required String name,
      required String email,
      required String password}) async {
    return Future.delayed(const Duration(seconds: 1), () {
      return true;
    });
  }

  Future<bool> loginUser(
      {required String email, required String password}) async {
    return Future.delayed(const Duration(seconds: 1), () {
      return true;
    });
  }

  Future<Map> getUserData({required String uid}) async {
    return Future.delayed(const Duration(seconds: 1), () {
      return {"": ""};
    });
  }

  Future<bool> postImage(File _image) async {
    return Future.delayed(const Duration(seconds: 1), () {
      return true;
    });
  }

  Future<void> logout() async {
    return Future.delayed(const Duration(seconds: 1), () {});
  }
}
